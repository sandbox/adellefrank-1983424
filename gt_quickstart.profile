<?php


/**
 * Implements hook_install_tasks_alter().
 */
function gt_quickstart_install_tasks_alter(&$tasks, &$install_state) {
  // Preselect the English language, so users can skip the language selection
  // form.
  if (!isset($_GET['locale'])) {
    $_POST['locale'] = 'en';
  }

}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function gt_quickstart_form_install_configure_form_alter(&$form, $form_state) {
    // Pre-populate the site configuration names with instructions and example values.
    // CHANGE These values to match your site's administrator information!!
    $form['site_information']['site_name']['#default_value'] = $_SERVER['HTTP_HOST'] . ' | Georgia Institute of Technology | Atlanta, GA';
    $form['site_information']['site_mail']['#default_value'] = 'admin@gatech.edu (Change this: Do NOT use an @mail.gatech.edu email, use another alias, such as George.Burdell@gatech.edu)';
    $form['admin_account']['account']['name']['#default_value'] = 'Rambling Reck (Change this: Do NOT use a GTaccount for User #1)';
    $form['admin_account']['account']['mail']['#default_value'] = 'admin@gatech.edu (Change this: Do NOT use an @mail.gatech.edu email, use another alias, such as George.Burdell@gatech.edu)';
    $form['server_settings']['site_default_country']['#default_value'] = 'US';
    $form['server_settings']['date_default_timezone']['#default_value'] = 'America/New_York';
    unset($form['server_settings']['date_default_timezone']['#attributes']['class']);
}

/**
 * Implements hook_install_tasks().
 */
function gt_quickstart_install_tasks($install_state) {

  $tasks = array(
      'gt_quickstart_run_batch' => array(
        'display_name' => st('GT configuration'),
        'display' => TRUE,
        'type' => 'batch',
        'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
        'function' => 'gt_quickstart_run_batch',
      ),
    );
  return $tasks;
}


/****************************
 * Additional functions     *
 ****************************/

/* Create batches for queued processing during GT configuration.
 * Helps prevent memory shortage during installation.
 */
function gt_quickstart_run_batch(&$install_state) {
    
  /*
   * Any variables that need to persist throughout the batch process 
   * must be set inside the $context array for each operation (sub-function),
   * because only that persists throughout the batch process.
   * 
   * Have each sub-function store results in $context['sandbox'] 
   * for the next sub-function to process.
   * 
   *  All sub-functions take &$context parameter by reference as last argument. 
   *  Note that this parameter should be session variable safe (no objects).
   * 
   *  The ORDER in which functions are done matters a lot, because some
   *  of these functions depend on other settings already being there 
   *  before they are run.
  */

    $operations = array(
        array('gt_quickstart_config_dates', array()),
        array('gt_quickstart_config_content', array()),
        array('gt_quickstart_config_menus', array()),
        array('gt_quickstart_config_theme_blocks', array()),
        array('gt_quickstart_config_cas', array()),
        array('gt_quickstart_config_advanced_users', array()),
        array('gt_quickstart_config_modules', array()),
        array('gt_quickstart_config_variables', array()),
        array('gt_quickstart_config_editor', array()),
    );

    $batch = array(
        'title' => t('GT settings configuration'),
        'operations' => $operations,
        'init_message' => st('GT configuration is starting.'),
        'progress_message' => st('Processed @current out of @total.'),
        'error_message' => st('GT configuration has encountered an Error!'),
        'finished' => 'gt_quickstart_batch_finished',
        'file' => drupal_get_path('profile', 'gt_quickstart') . '/gt_quickstart.profile',
    );
    
    return $batch;
}

/**
 * Function to output more detailed errors for batch 
 */
function gt_quickstart_batch_finished($success, $results) {
  /** The 'success' parameter means no fatal PHP errors were detected. All
   *  other error management should be handled using $context['results']. 
   */
  if ($success) {
    // Here we do something meaningful with the results.
    $message = 'GT Configuration finished.';
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    //$error_operation = $operations;
    //$message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    $message = t('An ERROR occurred while processing GT Configuration batch.');
    $message .= theme('item_list', $results);  // D6 syntax
    drupal_set_message($message, 'error');
    
  }
 
}


/**
 * Configure default menus.
 */
function gt_quickstart_config_menus(&$context) {

    $context['message'] = t('Build menus.');
    
    // Setup Main Menu links

    $item = array(
        'menu_name' => 'main-menu',
        'link_title' => st('Home'),
        'link_path' => 'node/1',
        'weight' => 0,
    );
    menu_link_save($item);

    $item = array(
        'menu_name' => 'main-menu',
        'link_title' => st('About'),
        'link_path' => 'node/2',
        'weight' => 4,
    );
    menu_link_save($item);

    $item = array(
        'menu_name' => 'main-menu',
        'link_title' => st('Contact'),
        'link_path' => 'contact',
        'weight' => 49,
    );
    menu_link_save($item);

    // Setup GATECH Utility links.
    $utility_menu = array(
        'menu_name' => 'menu-utility-menu',
        'title' => t('Utility menu'),
        'description' => t('The <em>Utility</em> menu contains default links for GT Drupal 7 theme.'),
    );
    menu_save($utility_menu);

    $item = array(
        'menu_name' => 'menu-utility-menu',
        'link_title' => st('Directories'),
        'link_path' => 'http://www.gatech.edu/directories',
        'weight' => 46,
    );
    menu_link_save($item);


    $item = array(
        'menu_name' => 'menu-utility-menu',
        'link_title' => st('Campus Map'),
        'link_path' => 'http://map.gtalumni.org/',
        'weight' => 47,
    );
    menu_link_save($item);

    $item = array(
        'menu_name' => 'menu-utility-menu',
        'link_title' => st('Buzzport'),
        'link_path' => 'http://buzzport.gatech.edu',
        'weight' => 48,
    );
    menu_link_save($item);

    // Setup Editor Menu links.
    $editor_menu = array(
        'menu_name' => 'menu-editor-menu',
        'title' => t('My Links'),
        'description' => t('The <em>Editor</em> menu contains default links for editing site content.'),
    );
    menu_save($editor_menu);

    $item = array(
        'menu_name' => 'menu-editor-menu',
        'link_title' => st('Find &amp Add content'),
        'link_path' => 'admin/content',
        'weight' => -3,
    );
    menu_link_save($item);

    $item = array(
        'menu_name' => 'menu-editor-menu',
        'link_title' => st('Add page'),
        'link_path' => 'node/add/page',
        'weight' => -9,
    );
    menu_link_save($item);

    $item = array(
    'menu_name' => 'menu-editor-menu',
    'link_title' => st('Add or change Event'),
    'link_path' => 'http://hg.gatech.edu/cas?destination=front_page',
    'weight' => -7,
    );
    menu_link_save($item);

    $item = array(
        'menu_name' => 'menu-editor-menu',
        'link_title' => st('Edit menu links'),
        'link_path' => 'admin/structure/menu/manage/main-menu',
        'weight' => -4,
    );
    menu_link_save($item);

    $item = array(
        'menu_name' => 'menu-editor-menu',
        'link_title' => st('Move blocks'),
        'link_path' => 'admin/structure/block',
        'weight' => 5,
    );
    menu_link_save($item);

    $item = array(
        'menu_name' => 'menu-editor-menu',
        'link_title' => st('Add block'),
        'link_path' => 'admin/structure/block/add',
        'weight' => 4,
    );
    menu_link_save($item);

    // Update the menu router information.
    menu_rebuild();
    
    $context['finished'] = 1;
}

/**
 * Configure default user roles and permissions.
 */
function gt_quickstart_config_advanced_users(&$context) {
    
    $context['message'] = t('Add user permissions.');

    // Set additional system permissions for EDITORS.
    $edit_perms = array(
        'access site in maintenance mode',
        'access contextual links',
        'administer blocks',
        'administer menu',
        'use text format text_editor_basic',
        'create page content',
        'edit any page content',
        'override page published option',
        'view revisions',
        'view revision status messages',
        'edit revisions',
        'publish revisions',
        'revert revisions',
        'unpublish current revision',
        'publish revisions of any page content',
        'view revisions of any page content',
        'access content overview'
        );

    // Set additional system permissions for site ADMINISTRATORS.
    $admin_perms = array(
        'access site in maintenance mode',
        'access contextual links',
        'administer blocks',
        'administer menu',
        'use text format text_editor_basic',
        'create page content',
        'edit any page content',
        'override page published option',
        'view revisions',
        'view revision status messages',
        'edit revisions',
        'publish revisions',
        'revert revisions',
        'unpublish current revision',
        'publish revisions of any page content',
        'view revisions of any page content',
        'access content overview',
        'administer nodes',
        'access administration pages',
        'view advanced help index',
        'view advanced help popup',
        'view advanced help topic',
        'access site reports',
        'access user profiles',
        'flush caches',
        'display drupal links',
        'access administration menu',
        'use text format text_editor_advanced',
        'use text format unfiltered_html',
        'create url aliases',
        'delete any page content',
        'delete revisions'
        );

    
    // Enable additional permissions for Editor and Administrator roles
    
    $roles = user_roles(TRUE);
    $rid_a = array_search('Administrator', $roles);
    $rid_e = array_search('Editor', $roles);

    user_role_grant_permissions($rid_e, $edit_perms);
    user_role_grant_permissions($rid_a, $admin_perms);
    
    $context['finished'] = 1;
    
}

/**
 * Configure default themes and blocks.
 */
function gt_quickstart_config_theme_blocks(&$context) {
     
    $context['message'] = t('Set themes and blocks.');
    
    //Set theme variables
    $default_theme = 'STARTERKIT';
    $admin_theme = 'seven';
    
    // Enable some standard blocks.
    // Must set blocks BEFORE enable themes, or the automatic
    // block installs by themes will conflict.

    $values = array(
        array(
            'module' => 'system',
            'delta' => 'main',
            'theme' => $default_theme,
            'status' => 1,
            'weight' => 0,
            'region' => 'content',
            'pages' => '',
            'title' => '',
            'cache' => -1,
        ),
        array(
            'module' => 'system',
            'delta' => 'main',
            'theme' => $admin_theme,
            'status' => 1,
            'weight' => 0,
            'region' => 'content',
            'pages' => '',
            'title' => '',
            'cache' => -1,
        ),
        array(
            'module' => 'system',
            'delta' => 'help',
            'theme' => $default_theme,
            'status' => 1,
            'weight' => 0,
            'region' => 'content_lead',
            'pages' => '',
            'title' => '<none>',
            'cache' => -1,
        ),
        array(
            'module' => 'system',
            'delta' => 'help',
            'theme' => $admin_theme,
            'status' => 1,
            'weight' => 0,
            'region' => 'help',
            'pages' => '',
            'title' => '<none>',
            'cache' => -1,
        ),
        array(
            'module' => 'search',
            'delta' => 'form',
            'theme' => $default_theme,
            'status' => 1,
            'weight' => 0,
            'region' => 'utility',
            'pages' => '',
            'title' => '<none>',
            'cache' => -1,
        ),
        array(
            'module' => 'menu',
            'delta' => 'menu-utility-menu',
            'theme' => $default_theme,
            'status' => 1,
            'weight' => 9,
            'region' => 'utility',
            'pages' => '',
            'title' => '<none>',
            'cache' => -1,
        ),
        array(
            'module' => 'system',
            'delta' => 'main-menu',
            'theme' => $default_theme,
            'status' => 1,
            'weight' => -9,
            'region' => 'left_nav',
            'pages' => '',
            'title' => '<none>',
            'cache' => -1,
        ),
        array(
            'module' => 'menu',
            'delta' => 'menu-editor-menu',
            'theme' => $default_theme,
            'status' => 1,
            'weight' => 0,
            'region' => 'left',
            'pages' => '',
            'title' => '',
            'cache' => -1,
        ),
    );
    $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'title', 'cache'));
    foreach ($values as $record) {
        $query->values($record);
    }
    $query->execute();
    
    // Tell system about default and admin themes
    theme_enable(array($default_theme, $admin_theme));

    variable_set('theme_default', $default_theme);
    variable_set('admin_theme', $admin_theme);

    // Disable some default themes
    theme_disable(array('bartik', 'garland', 'zen')); 
    
    $context['finished'] = 1;
}

/**
 * Configure CAS for GT
 */
function gt_quickstart_config_cas(&$context) {
    
    $context['message'] = t('Set CAS login settings.');

    variable_set('cas_access', 0);
    variable_set('cas_auto_assigned_role', array(
        1 => 0,
        2 => TRUE,
        3 => 0,
        4 => 0,
        5 => 0,
            )
    );
    variable_set('cas_cert', '');
    variable_set('cas_changePasswordURL', 'https://passport.gatech.edu');
    variable_set('cas_check_first', 0);
    variable_set('cas_debugfile', '');
    variable_set('cas_domain', 'mail.gatech.edu');
    variable_set('cas_exclude', 'services/*');
    variable_set('cas_first_login_destination', '');
    variable_set('cas_hide_email', 1);
    variable_set('cas_hide_password', 1);
    variable_set('cas_library_dir', 'sites/all/libraries/CAS');
    variable_set('cas_login_drupal_invite', '');
    variable_set('cas_login_form', '2');
    variable_set('cas_login_invite', 'Log in with your GTaccount');
    variable_set('cas_login_message', '');
    variable_set('cas_login_redir_message', 'You will be redirected to the secure GT login page.');
    variable_set('cas_logout_destination', 'https://login.gatech.edu/cas/login');
    variable_set('cas_pages', 'user*
admin/*
node/add/*
forms/*
node/*/edit
node/*/webform-results
contact');
    variable_set('cas_pgtformat', 'plain');
    variable_set('cas_pgtpath', '');
    variable_set('cas_port', '443');
    variable_set('cas_proxy', 0);
    variable_set('cas_registerURL', 'http://passport.gatech.edu');
    variable_set('cas_server', 'login.gatech.edu');
    variable_set('cas_uri', 'cas');
    variable_set('cas_user_register', 1);
    variable_set('cas_version', '2.0');
    
    $context['finished'] = 1;
}

/**
 * Configure default text editor & file management.
 */
function gt_quickstart_config_editor(&$context) {
    
    $context['message'] = t('Enable GT Editor.');
 
    /*
    * Enable GT Editor module and its dependencies.
    */

    module_enable(array('gt_editor'), TRUE);
    
    $context['finished'] = 1;

}

/**
 * Configure other important default variables for GT
 */
function gt_quickstart_config_variables(&$context) {

    $context['message'] = t('Enable key settings.');

    //Set important variables
    variable_set('site_frontpage', 'node/1');
    variable_set('site_403', 'node/3');
    variable_set('site_404', 'node/4');
    variable_set('update_check_frequency', 7);
    variable_set('update_notification_threshold', 'security');
    variable_set('update_check_disabled', 0);
    variable_set('pathauto_node_pattern', '[node:title]');
    variable_set('pathauto_case', 1);
    variable_set('pathauto_reduce_ascii', 1);
    variable_set('pathauto_separator', '-');
    variable_set('pathauto_transliterate', 1);
    variable_set('pathauto_update_action', 1);
    variable_set('globalredirect_settings', array('canonical',1));
    variable_set('preprocess_css', 1);
    variable_set('preprocess_js', 1);
    variable_set('page_compression', 1);
    variable_set('page_cache_maximum_age', 0);
    variable_set('dblog_row_limit', 10000);
    variable_set('date_api_use_iso8601', 1);
    variable_set('date_default_timezone', 'America/New_York');
    variable_set('date_first_day', 1);
    
    //Set file and image management variables
    variable_set('image_jpeg_quality', '100');
    variable_set('image_toolkit', 'gd');
    variable_set('imce_settings_absurls', '0');
    variable_set('imce_settings_disable_private', '1');
    variable_set('imce_settings_replace', '1');
    variable_set('imce_settings_thumb_method', 'scale_and_crop');
    variable_set('transliteration_file_lowercase', '1');
    variable_set('transliteration_file_uploads', '1');
    variable_set('transliteration_search', '1');

}

/*
 * Disable modules that are only installed for ease of migration
 * into Installatron and enable any other recommended modules.
 */
function gt_quickstart_config_modules(&$context) {

    $context['message'] = t('Disable unnecessary & Enable needed modules.');

    $modules = array(
        'comment' => 0,
        'image' => 0,
        'locale' => 0,
        'shortcut' => 0,
        'taxonomy' => 0,
        'auto_entitylabel' => 1,
        'field_permissions' => 1,
        'module_filter' => 1,
        'globalredirect' => 1,
        'navigation404' => 1,
        'token' => 1,
        'views' => 1,
        'views_bulk_operations' => 1,
        'views_data_export' => 1,
        'views_ui' => 1,
        'ctools' => 1,
        'diff' => 1,
        'entity' => 1,
        'libraries' => 1,
        'pathauto' => 1,
        'pathologic' => 1,
        'imce' => 1,
        'transliteration' => 1,
    );

    $context['sandbox']['max'] = count($modules);

    if (!isset($context['sandbox']['progress'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['current_module'] = 0;
    }

    // enable/disable each module and then update context variables.
    foreach ($modules as $module => $state):
        $context['sandbox']['current_module']++;
        if ($state == 1):
            if (!module_exists($module)):
                module_enable(array($module), FALSE);
            endif;
        else:
            if (module_exists($module)):
                module_disable(array($module), FALSE);
            endif;
        endif;
        $context['sandbox']['progress']++;
        if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
            $context['finished'] = $context['sandbox']['current_module'] / $context['sandbox']['max'];
        } else {
            $context['finished'] = 1;
        }
    endforeach;
}


/**
 * Create default page content for GT
 */
function gt_quickstart_config_content(&$context) {
    
  $context['message'] = t('Create some key pages of content.');    

/* Create home page */
  $node = new stdClass();
  $node->type = 'page';
  node_object_prepare($node);

  $node->title    = 'Home';
  $node->language = LANGUAGE_NONE;
  $node->uid    = 1;

  $node->body[$node->language][0]['value']   = '<p>Here is where your homepage content will go.</p>';
  $node->body[$node->language][0]['summary'] = '';
  $node->body[$node->language][0]['format']  = 'text_editor_basic';

  $node->path = array('alias' => 'home');

  node_save($node);

/* Create about page */
  $node = new stdClass();
  $node->type = 'page';
  node_object_prepare($node);

  $node->title    = 'About';
  $node->language = LANGUAGE_NONE;
  $node->uid    = 1;

  $node->body[$node->language][0]['value']   = '<p>We are awesome - learn more about us!</p>';
  $node->body[$node->language][0]['summary'] = '';
  $node->body[$node->language][0]['format']  = 'text_editor_basic';

  $node->path = array('alias' => 'about');

  node_save($node);

/* Create 403 error page */
  $node = new stdClass();
  $node->type = 'page';
  node_object_prepare($node);

  $node->title    = 'Our Apologies (403 error: permissions issue)';
  $node->language = LANGUAGE_NONE;
  $node->uid    = 1;

  $node->body[$node->language][0]['value']   = '<p>The page you requested does not recognize your permissions to view it, and may be part of a secured content area.</p>
<p>Please re-check the URL (web address) or <a href="contact" rel="nofollow">contact us</a> to tell us about a broken link.</p>
';
  $node->body[$node->language][0]['summary'] = '';
  $node->body[$node->language][0]['format']  = 'text_editor_basic';

  $node->path = array('alias' => '403');

  node_save($node);

/* Create 404 error page */
  $node = new stdClass();
  $node->type = 'page';
  node_object_prepare($node);

  $node->title    = 'Our Apologies (404 error: page not found)';
  $node->language = LANGUAGE_NONE;
  $node->uid    = 1;

  $node->body[$node->language][0]['value']   = '<h3>The page you are looking for is not here.</h3>
<p>We often update the web site in an effort to help you find information quickly and easily. The page you have requested may have moved, is temporarily unavailable, or is part of a secured content area.</p>
<p>Please re-check the URL (web address) or <a href="contact" rel="nofollow">contact us</a> to tell us about a broken link.</p>
';
  $node->body[$node->language][0]['summary'] = '';
  $node->body[$node->language][0]['format']  = 'text_editor_basic';

  $node->path = array('alias' => '404');

  node_save($node);

/* Create blog landing page and unpublish */
  $node = new stdClass();
  $node->type = 'page';
  node_object_prepare($node);

  $node->title    = 'Blog';
  $node->language = LANGUAGE_NONE;
  $node->uid    = 1;

  $node->body[$node->language][0]['value']   = '<p>This could be your blog landing page.</p>';
  $node->body[$node->language][0]['summary'] = '';
  $node->body[$node->language][0]['format']  = 'text_editor_basic';

  $node->path = array('alias' => 'blog');

  $node->status    = 0; // 0 is UNpublished.

  node_save($node);

}

/**
* Implements hook_date_formats().
*/
function gt_quickstart_config_dates(&$context) {
    
  $context['message'] = t('Set date formats.');  
    
  $formats = array(
    array(
     'type' => 'full_year_month_weekday',
     'title' => 'Date, with no time. Ex: 2013 April 19',
     'format' => 'Y F j', // 2013 April 19
     'locales' => array(),
     ),
    array(
     'type' => 'full_year_month',
     'title' => 'Full Year and then month. Ex: 2013 April',
     'format' => 'Y F', // 2013 April
     'locales' => array(),
     ),
    array(
     'type' => 'short_weekday_month_day',
     'title' => 'Day and Month. Ex: Fri Apr 19',
     'format' => 'D M j', // Fri Apr 19
     'locales' => array(),
     ),
    array(
     'type' => 'formal_usa',
     'title' => 'Formal USA. Ex: April 19, 2013',
     'format' => 'F j, Y', // April 19, 2013
     'locales' => array(),
     ),
    array(
     'type' => 'short_usa',
     'title' => 'Short USA. Ex: 04/19/2013',
     'format' => 'm/d/Y', // 04/19/2013
     'locales' => array(),
     ),
    array(
     'type' => 'time_only',
     'title' => 'Time only. Ex: 8:06 pm',
     'format' => 'g:i a', // 8:06 pm
     'locales' => array(),
     ),
    array(
     'type' => 'year_only',
     'title' => 'Year only. Ex: 2013',
     'format' => 'Y', // 2013
     'locales' => array(),
     ),
    array(
     'type' => 'quick',
     'title' => 'Quick. Ex: Fri Apr 19 8:06 pm',
     'format' => 'D M j g:i a', // Fri Apr 19 8:06 pm
     'locales' => array(),
     ),
  );

  // Set each date FORMAT as a variable
  foreach ($formats as $format) {
    variable_set('date_format_' . $format['type'], $format['format']);
  }

 // Insert each date format into the date_formats Table  
 foreach ($formats as $format) {
   db_insert('date_formats')->fields(array(
      'type' => $format['type'],  // Machine Name
      'format' => $format['format'], // Display Name
      'locked' => 0,        // 1 = can't change through UI, 0 = can change
    ))
    ->execute();
  }
  
  // Insert each date format TYPE into the date_format_type Table
  
  foreach ($formats as $format) {
   db_insert('date_format_type')->fields(array(
      'type' => $format['type'],  // Machine Name
      'title' => $format['title'], // Display Name
      'locked' => 0,        // 0 = provided by user, 1 = provided by system
    ))
    ->execute();
  }
  
}


